package ru.webanimal.academy_lesson01_two_activities;

import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //==============================================================================================
    // Widgets
    //==============================================================================================

    private TextInputEditText editText;


    //==============================================================================================
    // Activity callbacks
    //==============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolBar));
        setTitle(getString(R.string.activity_main_title));

        editText = findViewById(R.id.inputText);
    }

    @Override
    protected void onResume() {
        super.onResume();

        editText.setText(restoreFromSharedPreferences());
        setListeners(true);
    }

    @Override
    protected void onPause() {
        storeToSharedPreferences();
        setListeners(false);
        super.onPause();
    }


    //==============================================================================================
    // View.OnClickListener callback
    //==============================================================================================

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fab:
                SendMessageActivity.startActivity(this, getInputText());
                break;

            case R.id.clearButton:
                clearInputText();
                break;
        }
    }


    //==============================================================================================
    // Private methods
    //==============================================================================================

    private void setListeners(boolean nonNull) {
        findViewById(R.id.fab).setOnClickListener(nonNull ? this : null);
        findViewById(R.id.clearButton).setOnClickListener(nonNull ? this : null);
    }

    private String getInputText() {
        return editText.getText().toString();
    }

    private void clearInputText() {
        editText.setText("");
        storeToSharedPreferences();
    }

    private void storeToSharedPreferences() {
        SharedPreferences sp = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        sp.edit().putString("preference_input_message", getInputText()).apply();
    }

    private String restoreFromSharedPreferences() {
        return getSharedPreferences(
                getPackageName(),
                MODE_PRIVATE).getString("preference_input_message", "");
    }
}
