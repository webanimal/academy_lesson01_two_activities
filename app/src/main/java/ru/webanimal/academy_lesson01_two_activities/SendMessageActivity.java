package ru.webanimal.academy_lesson01_two_activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

public class SendMessageActivity extends AppCompatActivity implements View.OnClickListener {

    //==============================================================================================
    // Static fields
    //==============================================================================================

    private static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";


    //==============================================================================================
    // Fields
    //==============================================================================================

    private String message;


    //==============================================================================================
    // Static methods
    //==============================================================================================

    public static void startActivity(Context context, String message) {
        Intent intent = new Intent(context, SendMessageActivity.class);
        intent.putExtra(EXTRA_MESSAGE, TextUtils.isEmpty(message) ?
                context.getString(R.string.activity_send_message_text) : message);
        context.startActivity(intent);
    }


    //==============================================================================================
    // Activity callbacks
    //==============================================================================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message_activity);
        setSupportActionBar(findViewById(R.id.toolBar));
        setTitle(getString(R.string.activity_send_message_title));

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        message = getIntent().getStringExtra(EXTRA_MESSAGE);
        ((AppCompatTextView)findViewById(R.id.messageText)).setText(message);
    }

    @Override
    protected void onResume() {
        super.onResume();

        findViewById(R.id.fab).setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        findViewById(R.id.fab).setOnClickListener(null);
    }


    //==============================================================================================
    // View.OnClickListener callback
    //==============================================================================================

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fab:
                sendMessage();
                break;
        }
    }


    //==============================================================================================
    // Private methods
    //==============================================================================================

    private void sendMessage() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(getString(R.string.send_mail_intent_type));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.send_mail_default_email)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.send_mail_default_subject));
        intent.putExtra(Intent.EXTRA_TEXT, message);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent, getString(R.string.send_mail_chooser_title)));

        } else {
            Toast.makeText(this,
                    getString(R.string.alert_send_message_application_not_found),
                    Toast.LENGTH_LONG).show();
        }
    }
}
